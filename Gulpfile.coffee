browserSync = require('browser-sync')
gulp        = require('gulp')
gutil       = require('gulp-util')
coffee      = require("gulp-coffee")
sass        = require('gulp-sass')
uglify      = require('gulp-uglify-es').default
zip         = require('gulp-zip')
concat      = require('gulp-concat')
rename      = require('gulp-rename')
CSON = require 'cson'
fs = require 'fs'
glob = require 'glob'

reload = browserSync.reload

html = (target, debug) ->
  return () ->
    sources = [
      'html/index.html'
      'html/ru.html'
    ]
    #if (debug)
    #  sources.push('html/test.html')
    gulp.src(sources)
      .pipe(gulp.dest(target))
    #gulp.src(['game/gamepad.min.js']).pipe(gulp.dest(target+"/game"))
    gulp.src(['node_modules/salet/lib/index.min.js'])
      .pipe(rename('salet.min.js'))
      .pipe(gulp.dest(target+"/game"))

# Images
img = (target) ->
  return () ->
    return gulp.src(['img/*.png', 'img/*.jpeg', 'img/*.jpg']).pipe(gulp.dest(target))

# Audio assets
audio = (target) ->
  return () ->
    return gulp.src(['audio/*.mp3']).pipe(gulp.dest(target))

gulp.task('html', html('./build', true))
gulp.task('img', img('./build/img'))
gulp.task('audio', audio('./build/audio'))

# SCSS styles
gulp.task('sass', () ->
  gulp.src('sass/main.scss')
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(gulp.dest('./build/css'))
)

# Autotests
gulp.task('tests', () ->
  gulp.src('test/*.js')
    .pipe(gulp.dest('./build/test/'))
)

gulp.task('concatCoffee', () ->
  cson('./build')
  gulp.src([
    #procgen
    './build/game/procgen.coffee'
    # language
    './game/language/ru.coffee'
    './game/language/en.coffee'
    # Improv
    './game/improv/index.coffee'
    './game/improv/filters.coffee'
    './game/improv/template.coffee'
    ## additional functions
    "./lib/oneOf.coffee"
    "./lib/phrase.coffee"
    "./lib/dialogue.coffee"
    ## the actual game
    './game/begin.coffee'
    './game/story.coffee'
  ]).pipe(concat('./main.coffee'))
    .pipe(gulp.dest('./build/game'))
)

cson = (target) ->
  sourcedir =
    "game/procgen/en/": "en"
    "game/procgen/ru/": "ru"
  outtext = "procgendata = {}\n"
  for sdir, lang of sourcedir
    files = glob.sync(sdir+'*.cson')
    spec = {}
    for file in files
      data = CSON.parseCSONFile(file)
      if not data.groups?
        data.groups = []
      if data.phrases?
        data.groups.push({
          tags: [],
          phrases: data.phrases
        })
      data.phrases = null
      key = file.substr(0, file.lastIndexOf('.')) || file
      key = key.replace(sdir, '')
      spec[key] = data
    source = CSON.stringify(spec).split("\n")
    out = ""
    for line in source
      out += "  "+line+"\n"
    source = "procgendata.#{lang} = {\n"+out+"}\n"
    outtext += source
  fs.writeFileSync(target+'/game/procgen.coffee', outtext)

gulp.task('html', html('./build', true))

gulp.task('coffee', ['concatCoffee'], () ->
  gulp.src('./build/game/main.coffee')
    .pipe(coffee({bare: true}))
    .pipe(gulp.dest('./build/game/'))
)

gulp.task('build', [
  'html',
  'img',
  'sass',
  'coffee',
  'audio',
  'tests'
])

gulp.task('serve', ['build'], () ->
  browserSync({
    server: {
      baseDir: 'build'
    }
    online: true
    browser: []
    ghostMode: false
  })

  sassListener = () ->
    reload('./build/css/main.css')

  gulp.watch(['./html/*.html'], ['html'])
  gulp.watch(['./sass/*.scss'], ['sass'])
  gulp.watch(['./img/*.png', './img/*.jpeg', './img/*.jpg'], ['img'])
  gulp.watch([
    './game/*.coffee'
    './game/translations/*.cson'
  ], ['coffee'])

  gulp.watch(['./build/css/main.css'], sassListener)
  gulp.watch(
    ['./build/game/bundle.js', './build/img/*', './build/index.html'],
    browserSync.reload)
)

gulp.task('html-dist', html('./dist', false))
gulp.task('img-dist', img('./dist/img'))
gulp.task('audio-dist', audio('./dist/audio'))
gulp.task('legal-dist', () ->
  return gulp.src(['LICENSE.txt'])
         .pipe(gulp.dest("./dist"))
)
gulp.task('sass-dist', () ->
  return gulp.src('./sass/main.scss')
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(gulp.dest('./dist/css'))
)

gulp.task('coffee-dist', ['concatCoffee'], () ->
  gulp.src('./build/game/main.coffee', {sourcemaps: false})
    .pipe(coffee())
    .pipe(uglify())
    .on('error', gutil.log)
    .pipe(gulp.dest('./dist/game/'))
)

gulp.task('dist', [
  'html-dist'
  'img-dist'
  'sass-dist'
  'coffee-dist'
  'audio-dist'
  'legal-dist'
])

gulp.task('zip', ['dist'], () ->
  return gulp.src('dist/**')
    .pipe(zip('dist.zip'))
    .pipe(gulp.dest('.'))
)
