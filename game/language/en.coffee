en_lang =
  intro: """
    # Stowaway tragedy
     
    <small>BEWARE: the game was translated by a dumb bot and a very tired human</small>
   
    It was a cold gray November evening.
    The Moscow-Vladivostok train arrived on the fourth platform at exactly 17pm.

    You did not have a ticket.
    But you really needed to go east.
    So, when the gloomy conductor was distracted for a moment, you slipped into the car and
    hid in one of the compartments.

    There were still twenty-four stops and a day's journey until Irkutsk.
    The train moved off quietly.
  """
  window: "The pillars flash by in {{the window.}}"
  dsc: """
      ##### In a train compartment

      It is a small compartment for four people.
    """
  alexey: (ImprovEngine, state) ->
    """
      On the top shelf in front of you sits {{#{if salet.character.knows_alexey then "Alex" else "an unshaven man in a dark tracksuit"}.}}
      #{ImprovEngine.gen("alexey", state)}
    """
  alexey_end: """
    The man in a dark tracksuit rummages through the lower shelves.
  """
  margo: (ImprovEngine, state) ->
    """
      On the bottom shelf near you rests {{#{if salet.character.knows_margo then "Margo" else "a beautiful lady with a hand ferret and cynical eyes"}.}}
      #{ImprovEngine.gen("margo", state)}
    """
  margo_end: """
    The girl is looking for a ferret in his luggage.
    Bags, a few suitcases ... it stuck for a long time.
  """
  owindow: "You notice that the window {{is not closed.}}"
  dsc_ferret: """
    ##### In a train compartment

    It is a small compartment for four people.
    There are exactly four people here, if you can count yourself as human.
  """
  window_end: "You don't pay attention to a fierce snowstorm in {{the window.}}"
  owindow_end: "But you do notice that the window is still {{not completely closed.}}"
  window_open_no: "You are not stuffy. Why open a window?"
  window_open: """
    With a sharp force you open the window.
    A small snowfall starts in the coupe.
  """
  meet_alexey: """
    — Good evening.

    — Hello to you. I'm Alex.

    You introduce yourself.
  """
  meet_margo: """
    — Good evening.

    You introduce yourself. Lady burns your eyes and replies:

    — Margarita.

    — Very nice.

    — I hate you.

    You think that's enough for the introductions.
  """
  alexey_dsc: (ImprovEngine, state) -> """
    He is a simple little scruffy guy in a dark jumpsuit.
    His hair is dyed in blond, but you already can see the dark ends.
    Under the suit he wears a T-shirt with the inscription: "Green Dragon".

    #{ImprovEngine.gen("alexey", state)}
  """
  margo_dsc: (ImprovEngine, state) -> """
    #{ImprovEngine.gen("margo", state)}
  """
  margo_bye: """
     — Ok I will not distract you anymore.

    Margarita is pointedly silent in response.
  """
  alexey_bye: """
    I fall silent. The conversation quickly fades as if we did not talk at all.
  """
  alexey_mood: "How's the mood, Alex?"
  alexey_mood_answ: "Lousy. But that's OK, it's always lousy."
  alexey_speed: "What do you think, will soon come?"
  alexey_speed_answ: """
    — What do you think will come soon?

    — We will arrive as the come.
    They also have a schedule.
    You want to play cards?

    — Nope.
  """
  alexey_reaction: "He does not pay attention to you."
  margo_reaction: "She does not notice you, but her ferret is watching you."
  bye: "End the conversation"
  end: "Change your mind"
  story_call: """
    And then the door is heard loud voice:

    „Checking the tickets!”

    You look - you have nowhere to go.
    The coupe already comes to the conductor of the car.

    Alex closes the book and puts it on the table.
    „Here's your ticket…” - but the conductor stops him:

    „Later. It is better to leave the compartment, it will be between us.”

    “What?”

    The conductor frowns.

    „And take the girl. She does not want to see it.“

    „Uh ... yes.“

    He throws a surprised look at you and quickly disappeared behind the door.

    You do not have time to recover, as a conductor hits you in the forehead with his fist,
    and then all sounds suddenly cease.
  """
  killed2_option: "Look around"
  killed2: """
    A glare blinds you.

    You see the same compartment, but now the door is open, and no one is inside.

    Outside, the warm autumn landscape gave way to a blizzard.
    Are you already in Siberia?

    Where everything is suddenly gone?
    Upper shelves stacked, and on the table book by Stephen King.
  """
  killed3_option: "Get out of the compartment"
  killed3: """
    You come to the door, but you have something stops.
    You physically can not get out of the compartment.

    Angry, you knock on the door with his fist ... and your fist goes through it.
    Do not you feel resistance when the waving through the door.

    After a while you realize that the table, the floor, walls and shelves, too, does not restrict you.
    Still, you can not pull the trunk compartment on the borders.
    What the hell is this?

    Finally, the door opens, and in comes the coupe is already familiar to you Alex.
  """
  killed4_option_a: "Say hello to him"
  killed4_option_b: "Yell at him"
  killed4: """
    He does not notice you.
    Wait, that's not quite true: it passes **through** you and does not notice you.
  """
  killed4_a: () -> """
    — Hi, Alex.

    #{"killed4".l()}
  """
  killed4_b: () -> """
    — Can *you* tell me **what is going on here?!**

    #{"killed4".l()}
  """
  killed5_option: "Demand answers"
  killed5: """
    — What the hell?!

    You jump to Alex and try to shake it, but your hand pass through it.
    You shout, but he can not hear you.
    You are trying to strangle him - nothing comes out.

    The compartment includes Margarita and sits on his seat.

    In the hearts of you banging his fist on the table.
    To your surprise, the table jumps.

    You have an idea.
    You carefully look at the hand and touch the books,
    which lies on the table.

    You can slide it!
    Of course, you still disembodied ghost, but it is something.

    You matures revenge.
    But if you can not go, how to get the conductor to come here?
  """
  book: "There's {{a paperback book}} on the table."
  book_floor: "There's {{a paperback book}} on the floor."
  book_act: "Straining, you pick up a book and drop it on the table."
  book_act_twice: "The book just fell off the table, and you are so tired."
  reset_book: "Alex notices a book on the floor and bends down to pick it up."
  reset_book_ends: "Alex gets up and puts the book back."
  reset_window: """
    Margarita notices the open window and stretches to close it.
  """
  reset_window_ends: """
    Margarita closes the window down and took his former place.
  """
  blood_dsc: "You notice {{a red spot}} on the floor."
  blood_act: "It's blood. You are not very surprised."
  ferret_dsc: "Margarita's {{ferret}} is attentively looking at you."
  ferret_dsc_end: "{{The ferret}} Yulia is watching you from a dark corner."
  ferret_act: """
    Margarita does not let the ferret out of hand, but the little scoundrel can not wait to
    run.
  """
  ferret_one_margo: """
    The ferret embarks on a run, but the attentive hostess does not allow him to escape.
  """
  ferret_one_alexey: """
    The ferret embarks on a run, but your neighbor sees it and quickly catches.
  """
  ferret_run: """
    When both people are distracted, the ferret bullets flying off the shelf.
    He's hiding in the far corner, where you can see it well.

    When Margaret discovers the loss, it raises a cry.
    — Help! Julia! Julia! She could be anywhere in the car!

    Apparently, the ferret was a girl.
    Hostess goes, but after a while returned with a guide
    wagon.

    — I do speak, she could not run away.
    Let's look for first in your compartment.

    Just then you and I need a guide.
    How now to neutralize it ...?
  """
  conductor: """
    {{The conductor}} is examining the floor.
    He does not move, so you may not notice the ferret.
  """
  table: """
    {{The folding table}} is assembled to open the floor.
  """
  table_act: """
    You push down the table.
    You can not put it on the latch, so he immediately folds back.
  """
  alexey_final: """
    You push the table down right on Alex's head.
    He groans softly and piled up on the floor.
  """
  alexey_final_dsc: "On the floor, under the table is insensitive sportsman."
  margo_final: """
    The conductor jumps to catch a ferret, but accidentally hurt
    the girl who is trying to reach out to the latch of the window.

    She falls and hits his head against the wall. She is unconscious.
  """
  margo_final_dsc: """
    On a shelf lying unconscious lover of rare animals.
  """
  ferret_whereto: "You can lure the ferret out, but where to?"
  ferret_close: "Lure to the table"
  ferret_close_dsc: """
    The ferret makes two jumps to the table, but then turns around and hides back.
  """
  ferret_far: "Lure in the middle of the compartment"
  ferret_far_dsc: """
     The ferret jumps in the middle of the coupe, makes a circle and comes back in a dark corner,
     so anyone and not seen.
  """
  ferret_blood: "Lure to the pool of blood"
  ferret_blood_dsc: """
    The ferret does not want to run so far - there are too many people.
  """
  conductor_examine: """
    Angry guard who killed you, but does not know that you have become a ghost
    and now you will forever dwell in the mysterious four-bed compartment of a train from Moscow to Vladivostok.

    And if your plan succeeds, it will remain here with you.
  """
  final: """
    — Ah, there you are!

    The conductor goes to the ferret, but instead slips on a puddle of your blood.
    He falls and hits his head on the corner of the shelf. If it survives, it will be a long time to be ill.

    Revenge is done. You feel that you can get off the train.

    ### END
  """
