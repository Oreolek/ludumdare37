## REAL ROOM

room "start",
  clear: false
  before: (from) ->
    if (!from)
      return "intro".l()
  dsc: () ->
    if salet.character.ferret
      return "dsc_ferret".l()
    return "dsc".l()
  afterChoices: () ->
    if (
      salet.character.lastBeat() > 3 and
      salet.character.killed != true and
      salet.character.knows_margo and
      salet.character.knows_alexey
    )
      salet.character.killed = true
      salet.character.newBeat()
      salet.goTo("killed")

    if (salet.character.book and salet.character.lastBeat() == 2)
      salet.view.write "**"+"reset_book".l()+"**"

    if (salet.character.window and salet.character.lastBeat() == 2)
      salet.view.write "**"+"reset_window".l()+"**"

    if (salet.character.window and salet.character.lastBeat() > 3)
      salet.character.window = false
      salet.character.newBeat()
      salet.view.write "**"+"reset_window_ends".l()+"**"

    if (salet.character.book and salet.character.lastBeat() > 3)
      salet.character.book = false
      salet.character.newBeat()
      salet.view.write "**"+"reset_book_ends".l()+"**"
  canExit: (to) ->
    if salet.character.killed and (to == "margo" or to == "alexey")
      salet.view.clearContent()
      salet.view.append("<em>"+"#{to}_reaction".l()+"</em>")
      return false
  units:[
    cunit "window",
      order: 5
      dsc: () ->
        if salet.character.ferret
          return "\n\n"+"window_end".l()
        return "\n\n"+"window".l()
      enact: () ->
        return procgen("window")
    cunit "owindow",
      order: 6
      dsc: () ->
        if salet.character.ferret
          return "owindow_end".l()
        return "owindow".l()
      enact: () ->
        if salet.character.killed
          salet.character.window = true
          salet.character.newBeat()
          return "window_open".l()
        else
          return "window_open_no".l()
    cunit "alexey",
      order: 3
      dsc: () ->
        if salet.character.alexey
          return "alexey_final_dsc".l()
        if salet.character.ferret
          return "alexey_end".l()
        return "alexey".l(ImprovEngine, state)
      enact: () ->
        salet.goTo("alexey")
    cunit "margo",
      order: 4
      dsc: () ->
        if salet.character.margo
          return "margo_final_dsc".l()
        if salet.character.ferret
          return "margo_end".l()
        "margo".l(ImprovEngine, state)
      enact: () ->
        salet.goTo("margo")
  ]
    ###
    cunit "debug",
      dsc: "<center>{{Skip until the puzzles}}</center>"
      order: 666
      enact: () ->
        state.setTag("weather", "cold")
        salet.here().take(book)
        salet.here().take(blood)
        salet.here().take(ferret)
        salet.character.newBeat()
        salet.character["knows_alexey"] = true
        salet.character["knows_margo"] = true
        salet.here().drop("debug")
        salet.character.killed = true
        return "Done."
    ###

book = cunit "book",
  order: 7
  dsc: () ->
    if salet.character.book
      return "\n\n"+"book_floor".l()
    else
      return "\n\n"+"book".l()
  enact: () ->
    if salet.character.book
      return "book_act_twice".l()
    else
      salet.character.book = true
      salet.character.newBeat()
      return "book_act".l()

blood = cunit "blood",
  order: 8
  dsc: "blood_dsc".l()
  enact: "blood_act".l()

ferret = cunit "ferret",
  dsc: () ->
    if salet.character.ferret
      return "ferret_dsc_end".l()
    return "ferret_dsc".l()
  order: 4
  enact: () ->
    if salet.character.ferret
      return salet.goTo("ferret")
    if salet.character.window and salet.character.book
      # хорьку никто не мешает
      salet.character.book = false
      salet.character.window = false
      salet.character.ferret = true
      salet.here().take(conductor)
      salet.here().take(table)
      salet.view.clearContent()
      salet.here().entering("start")
      return "ferret_run".l()
    else if salet.character.window
      return "ferret_one_margo".l()
    else if salet.character.book
      return "ferret_one_alexey".l()
    else
      return "ferret_act".l()

conductor = cunit "conductor",
  dsc: "conductor".l()
  order: 2
  enact: "conductor_examine".l()

table = cunit "table",
  dsc: "table".l()
  order: 7
  enact: () ->
    if salet.character.book and !salet.character.alexey
      salet.character.alexey = true
      return "alexey_final".l()
    else
      return "table_act".l()

## ACTUALLY DIALOGUES

dlg = (name) ->
  return room(name, {
    before: () ->
      if salet.character["knows_#{name}"] != true
        salet.character["knows_#{name}"] = true
        return "meet_#{name}".l()
    choices: "##{name}",
    dsc: () -> 
      "#{name}_dsc".l(ImprovEngine, state)
  })

dlg_bye = (name) ->
  return room("#{name}_bye", {
    clear: false
    tags: [name]
    optionText: "bye".l()
    dsc: "#{name}_bye".l()
    after: () ->
      return salet.goTo("start")
  })

dlg("alexey")
dlg("margo")

dialogue "alexey_mood".l(), "alexey", "alexey", "alexey_mood_answ".l()
dialogue "alexey_speed".l(), "alexey", "alexey", "alexey_speed_answ".l()

dlg_bye("alexey")
dlg_bye("margo")

room "killed",
  clear: false
  choices: "#killed"
  dsc: "story_call".l()

room "killed2",
  clear: false
  optionText: "killed2_option".l()
  dsc: "killed2".l()
  tags: ["killed"]
  choices: "#killed"
  canView: () ->
    true if !salet.isVisited("killed2")

room "killed3",
  clear: false
  optionText: "killed3_option".l()
  dsc: "killed3".l()
  tags: ["killed"]
  choices: "#killed"
  canView: () ->
    true if salet.isVisited("killed2") and !salet.isVisited("killed3")

room "killed4_a",
  clear: false
  optionText: "killed4_option_a".l()
  dsc: "killed4_a".l()
  tags: ["killed"]
  choices: "#killed"
  canView: () ->
    true if salet.isVisited("killed3") and !salet.isVisited("killed4_b")

room "killed4_b",
  clear: false
  optionText: "killed4_option_b".l()
  dsc: "killed4_b".l()
  tags: ["killed"]
  choices: "#killed"
  canView: () ->
    true if salet.isVisited("killed3") and !salet.isVisited("killed4_a")

room "killed5",
  clear: false
  enter: () ->
    state.setTag("weather", "cold")
    salet.rooms["start"].take(book)
    salet.rooms["start"].take(blood)
    salet.rooms["start"].take(ferret)
  optionText: "killed5_option".l()
  dsc: "killed5".l()
  tags: ["killed"]
  canView: () ->
    true if salet.isVisited("killed4_a") or salet.isVisited("killed4_b")
  after: () ->
    return salet.goTo("start")

room "ferret",
  clear: true
  dsc: "ferret_whereto".l()
  choices: "#ferret"

room "ferret_close",
  clear: false
  optionText: "ferret_close".l()
  dsc: () -> 
    if salet.character.window and !salet.character.margo
      salet.character.margo = true
      return "margo_final".l()
    return "ferret_close_dsc".l()
  after: () ->
    salet.goTo("start")
  tags: ["ferret"]

room "ferret_blood",
  clear: false
  optionText: "ferret_blood".l()
  dsc: () ->
    if salet.character.alexey and salet.character.margo
      salet.goTo("final")
      return ""
    return "ferret_blood_dsc".l()
  tags: ["ferret"]
  after: () ->
    unless salet.character.alexey and salet.character.margo
      salet.goTo("start")

room "ferret_far",
  clear: false
  optionText: "ferret_far".l()
  dsc: "ferret_far_dsc".l()
  tags: ["ferret"]
  after: () ->
    salet.goTo("start")

room "final",
  clear: true
  dsc: "final".l()
