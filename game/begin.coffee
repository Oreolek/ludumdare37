
i18n.push("ru", ru_lang)
i18n.push("en", en_lang)

salet.game_id = "0ee0825d-0c71-4a08-bfe5-730e575df26d"
salet.game_version = "1.0"

lang = document.getElementsByTagName("html")[0].getAttribute("lang") || "en"
ImprovEngine = new Improv(procgendata[lang], {
  rng: () ->
    return salet.rnd.randf()
  filters: [
    Improv.filters.mismatchFilter()
  ]
  reincorporate: false
})

$(document).on("init", () ->
  salet.character.lastBeat = () ->
    return salet.progress.sequence.length - salet.character.beat
  salet.character.newBeat = () ->
    salet.character.beat = salet.progress.sequence.length
  salet.character.beat = 0
  state.setTag("weather", "warm")
  audio = document.getElementById("bgsound")
  if audio?
    audio.currentTime = 0
    audio.volume = 1.0
    audio.play()
)

$(document).ready(() ->
  window.addEventListener('popstate', (event) ->
    salet.goBack()
  )
  $("#night").on("click", () ->
    $("#night").toggleClass("active")
    $("body").toggleClass("night")
    if (window.night)
      window.night = false
    else
      window.night = true
  )

  salet.beginGame()
)

###
Element helpers. There is no real need to build monsters like a().id("hello")
because you won't use them as is. It does not make sense in context, the
author has Markdown and all utilities to *forget* about the markup.
###
way_to = (content, ref) ->
  return "<a href='#{ref}' class='way'>#{content}</a>"
textlink = (content, ref) ->
  return "<a href='./_writer_#{ref}' class='once'>#{content}</a>"
actlink = (content, ref) ->
  return "<a href='./#{ref}' class='once'>#{content}</a>"

class ImprovModel
  constructor: () ->
    @tags = []
  getTag: (tagName) ->
    for tag in @tags
      if tag[0] == tagName
        return tag[1]
    return undefined
  hasTag: (tagName) ->
    for tag in @tags
      if tag[0] == tagName
        return true
    return false
  setTag: (tagName, value) ->
    for tag, index in @tags
      if tag[0] == tagName
        @tags[index][1] = value
        return true
    @tags.push([tagName, value])
    return true
  setTagIfNotPresent: (tagName, value) ->
    if @getTag(tagName) == undefined
      @tags.push([tagName, value])
state = new ImprovModel

cunit = (name, spec) ->
  spec.act = () ->
    # re-enter the room, reroll the description
    salet.view.clearContent()
    r = salet.here()
    r.entering(r.name)
    return '<em>'+@enact.fcall(this)+'</em>'
  return unit(name, spec)
procgen = (tag) ->
  return ImprovEngine.gen(tag, state)
